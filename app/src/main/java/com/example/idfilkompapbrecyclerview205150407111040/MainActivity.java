package com.example.idfilkompapbrecyclerview205150407111040;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.idfilkompapbrecyclerview205150407111040.adapter.MahasiswaAdapter;
import com.example.idfilkompapbrecyclerview205150407111040.model.Mahasiswa;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMahasiswaListener {

    RecyclerView recyclerView;
    MahasiswaAdapter mahasiswaRecyclerAdapter;
    ArrayList<Mahasiswa> _mahasiswaList;
    Button btnAddData;
    EditText inputNama, inputNIM;
    RadioGroup optionsJenisKelamin;
    int[] foto = {R.drawable.ava_man, R.drawable.ava_woman};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputNama = findViewById(R.id.input_nama_edit_text);
        inputNIM = findViewById(R.id.input_NIM_edit_text);
        optionsJenisKelamin = findViewById(R.id.options_jenis_kelamin);

        btnAddData = findViewById(R.id.BtnAddData);
        btnAddData.setOnClickListener(this);
        loadData();
        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rvMahasiswa);
        mahasiswaRecyclerAdapter = new MahasiswaAdapter(_mahasiswaList, this, this);
        recyclerView.setAdapter(mahasiswaRecyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void addData(String nama, String NIM, int jenisKelamin) {

        if(jenisKelamin == 0)
        {
            _mahasiswaList.add(new Mahasiswa(nama, NIM, foto[0]));
        }
        else
        {
            _mahasiswaList.add(new Mahasiswa(nama, NIM, foto[1]));
        }

    }

    private void loadData() {
        _mahasiswaList = new ArrayList<>();
        _mahasiswaList.add(new Mahasiswa("Andi Firmansyah", "205150407001", foto[0]));
        _mahasiswaList.add(new Mahasiswa("Janda Kabul", "205150400001", foto[1]));
        _mahasiswaList.add(new Mahasiswa("Satpar Bundar", "205150407002", foto[0]));
        _mahasiswaList.add(new Mahasiswa("Mawar Bunga", "205150400002", foto[1]));

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnAddData.getId())
        {
            String nama = inputNama.getText().toString();
            String NIM = inputNIM.getText().toString();
            int jenisKelamin = 0;

            if(nama.equals("") || NIM.equals(""))
            {
                Toast.makeText(MainActivity.this,
                        "Nama dan NIM Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
                return;
            }

            switch (optionsJenisKelamin.getCheckedRadioButtonId())
            {
                case R.id.option_male: jenisKelamin = 0; break;
                case R.id.option_female: jenisKelamin = 1; break;
            }

            addData(nama, NIM, jenisKelamin);
            inputNama.setText("");
            inputNIM.setText("");
            mahasiswaRecyclerAdapter.notifyDataSetChanged();
        }
        hideKeyboard((Button)v);
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }catch(Exception ignored) {
        }
    }

    @Override
    public void onMahasiswaClick(int position) {
        Log.d(TAG, "onMahasiswaClick: clicked.");

        Intent intent = new Intent(this, BiodataActivity.class);
        intent.putExtra("keyNama", _mahasiswaList.get(position).get_nama());
        intent.putExtra("keyNIM", _mahasiswaList.get(position).get_NIM());
        intent.putExtra("keyFoto", _mahasiswaList.get(position).get_foto());
        startActivity(intent);
    }
}